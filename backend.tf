terraform {
  backend "remote" {
    hostname     = "app.terraform.io"
    organization = "terraform-multi"

    workspaces {
      name = "azure"
    }
  }
}